#from google.cloud import ndb
import logging
import key
import params
import bot_ux
import json
import api

class Person():#ndb.Model):
    #chat_id = ndb.IntegerProperty()
    #name = ndb.StringProperty()
    #last_name = ndb.StringProperty()
    #username = ndb.StringProperty()
    #state = ndb.StringProperty(indexed=True)
    #last_state  = ndb.StringProperty()
    ##last_mod = ndb.DateTimeProperty(auto_now=True)
    #enabled = ndb.BooleanProperty(default=True)
    #variables = ndb.JsonProperty(indexed=False)
    #language_interface = ndb.StringProperty(default=params.default_language_interface)
    #language_exercise = ndb.StringProperty(default=params.default_language_exercise)
    #ux_lang = None # see method ux()
    
    
    #chat_id
    #name
    #last_name
    #username
    #state
    #last_state
    #enabled
    #variables
    #language_interface
    #language_exercise
    #ux_lang = None # see method ux()
    
    def __init__(self, *args, **kwargs):
        self.chat_id = kwargs.get('chat_id')
        self.name = kwargs.get('name')
        self.last_name = kwargs.get('last_name')
        self.username = kwargs.get('username')
        self.variables = kwargs.get('variables')    
        self.state = kwargs.get('state')
        self.last_state = kwargs.get('last_state')
        self.enabled = kwargs.get('enabled',True)
        self.language_interface = kwargs.get('language_interface')
        self.language_exercise = kwargs.get('language_exercise')
        self.ux_lang = None
   
    def user_telegram_id(self):
        return 'telegram_{}'.format(self.chat_id)

    def ux(self):
        if self.ux_lang is None:
            self.ux_lang = bot_ux.UX_LANG(self.language_interface)
        return self.ux_lang

    def get_name(self):
        return self.name

    def get_last_name(self):
        return self.last_name if self.last_name else ''

    def get_username(self):
        return self.username

    def get_name_last_name(self):
        return self.get_name() + ' ' + self.get_last_name()

    def get_name_last_name_username(self):
        result = self.get_name_last_name()
        if self.username:
            result += ' @' + self.get_username()
        return result

    def set_enabled(self, enabled, put=False):
        self.enabled = enabled
        if put:
            self.puti()

    def switch_notifications(self, put=True):
        self.enabled = not self.enabled
        if put:
            self.puti()

    def update_user(self, name, last_name, username, put=False):
        import params
        changed = False
        if self.name!=name:
            self.name = name
            changed = True
        if self.last_name!=last_name:
            self.last_name = last_name
            changed = True
        if self.username!=username:
            self.username = username
            changed = True
        if self.language_exercise not in params.LANGUAGES:
            self.language_exercise = params.default_language_exercise
        if self.language_interface not in params.LANGUAGES:
            self.language_interface = params.default_language_interface
        if changed and put:
            self.puti()

    def set_language_interface(self, lang):
        self.language_interface = lang
        self.ux_lang = bot_ux.UX_LANG(lang)

    def set_language_exercise(self, lang):
        self.language_exercise = lang

    def set_state(self, newstate, put=True):
        self.last_state = self.state
        self.state = newstate
        if put:
            self.puti()

    def is_administrator(self):
        result = self.chat_id in key.ADMIN_IDS
        #logging.debug("Amministratore: " + str(result))
        return result

    def set_last_exercise_id_and_options(self, ex_id, options):
        self.set_variable('Exercise_ID', ex_id)
        self.set_variable('Exercise_Options', options)
        self.puti()

    def get_last_exercise_id_and_options(self):
        options = [x for x in self.get_variable('Exercise_Options')]
        return self.get_variable('Exercise_ID'), options

    def set_keyboard(self, kb, put=True):
        self.set_variable("keyboard", kb, put=put)

    def get_keyboard(self):
        return self.get_variable("keyboard", [])

    def set_variable(self, var_key, var_value, put=True):
        self.variables[var_key] = var_value
        if put:
            self.puti()
    
    def get_variable(self, var_key, default_value=None):
        return self.variables.get(var_key, default_value)

    def is_admin(self):
        import key
        return self.chat_id in key.ADMIN_IDS

    def is_manager(self):
        import key
        return self.chat_id in key.MANAGER_IDS
    def toJSON(self):
        if self.ux_lang is None:
             self.ux()
        return json.dumps({'chat_id': self.chat_id, 
                           "name": self.name, 
                           "last_name":  self.last_name, 
                           "username": self.username, 
                           "state": self.state, 
                           "last_state": self.last_state, 
                           #"last_mod": self.last_mod,
                           "enabled": self.enabled,
                           "variables": self.variables,
                           "language_interface": self.language_interface,
                           "language_exercise": self.language_exercise,
                           "ux_lang": self.ux_lang.toJson()})
    def puti(self):
        api.record_userdata(self.chat_id,self.toJSON())
        #self.put()

def add_person(chat_id, name, last_name, username,lang):
    from params import LANGUAGES
    
    language_keys = LANGUAGES.keys()
    if lang not in LANGUAGES.keys():
        lang = "EN"
    
    p = Person(
        #id=str(chat_id),
        chat_id=chat_id,
        name=name,
        last_name = last_name,
        username = username,
        variables = {},
        language_interface = lang,
        language_exercise = lang
    )   
    p.ux()
    api.add_userdata(p.chat_id,p.toJSON())
    #p.put()
    return p


def get_person_by_id(chat_id):
    userdata = api.get_userdata(chat_id)
    if userdata == None:
        return None;
    else:
        #userdata_json = json.loads(userdata)
        p = Person(
            #id=str(userdata['chat_id']),
            chat_id=userdata['chat_id'],
            name=userdata['name'],
            last_name=userdata['last_name'],
            username=userdata['username'],
            state=userdata['state'],
            last_state=userdata['last_state'],
            enabled=userdata['enabled'],
            variables=userdata['variables'],
            language_interface=userdata['language_interface'],
            language_exercise=userdata['language_exercise']
        )    
        return p
        #Person.get_by_id(str(chat_id))

