#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Vtrel bot

"""

import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)

logger = logging.getLogger(__name__)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error, context)
    
def all_handler(update, context):
    from main_exception import run_new_thread_and_report_exception
    from bot_telegram_dialogue import deal_with_request    
    import json
    logging.debug("TELEGRAM POST REQUEST: ",update)
    print('Update "%s"',update)
    run_new_thread_and_report_exception(deal_with_request, update)


def main():
    import key
    
    updater = Updater(key.TELEGRAM_TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    dp.add_handler(MessageHandler(Filters.all, all_handler))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
